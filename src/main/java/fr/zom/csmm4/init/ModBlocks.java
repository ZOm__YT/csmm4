package fr.zom.csmm4.init;

import com.google.common.collect.Lists;
import fr.zom.csmm4.ModCSMM;
import fr.zom.csmm4.blocks.BlockCSMM;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, modid = ModCSMM.MODID)
public class ModBlocks
{

	public static final ModBlocks INSTANCE = new ModBlocks();

	public static List<Block> blocks;
	public static List<BlockItem> blockItems;

	public static Block blue_emerald_block;

	private static void init()
	{
		blocks = Lists.newArrayList();
		blockItems = Lists.newArrayList();

		blue_emerald_block = new BlockCSMM("blue_emerald_block", Block.Properties.create(Material.IRON).hardnessAndResistance(5.0f, 20.0f).sound(SoundType.METAL));
	}

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> e)
	{

		init();

		for ( Block b : blocks )
		{
			e.getRegistry().register(b);
		}
	}

	@SubscribeEvent
	public static void registerBlockItems(RegistryEvent.Register<Item> e)
	{
		for ( BlockItem bi : blockItems )
		{
			e.getRegistry().register(bi);
		}
	}

}
