package fr.zom.csmm4.blocks;

import fr.zom.csmm4.init.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;

public class BlockCSMM extends Block
{
	public BlockCSMM(String name, Properties properties)
	{
		super(properties);
		setRegistryName(name);

		ModBlocks.blocks.add(this);
		ModBlocks.blockItems.add((BlockItem) new BlockItem(this, new Item.Properties()).setRegistryName(getRegistryName()));
	}
}
