package fr.zom.csmm4;

import fr.zom.csmm4.init.ModBlocks;
import fr.zom.csmm4.init.ModItems;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import java.util.logging.Logger;

@Mod(ModCSMM.MODID)
public class ModCSMM
{
	public static final String MODID = "csmm";

	public static final Logger logger = Logger.getLogger(MODID);

	public ModCSMM()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);

		MinecraftForge.EVENT_BUS.register(ModItems.INSTANCE);
		MinecraftForge.EVENT_BUS.register(ModBlocks.INSTANCE);
	}

	private void setup(final FMLCommonSetupEvent e)
	{
		logger.info("Mod Setup Loading !");
	}

	private void clientSetup(final FMLClientSetupEvent e)
	{
		logger.info("Mod Client Setup Loading !");
	}
}
