package fr.zom.csmm4.items;

import fr.zom.csmm4.init.ModItems;
import net.minecraft.item.Item;

public class ItemCSMM extends Item
{
	public ItemCSMM(String name)
	{
		super(new Properties());

		setRegistryName(name);

		ModItems.items.add(this);
	}
}
