package fr.zom.csmm4.init;

import com.google.common.collect.Lists;
import fr.zom.csmm4.ModCSMM;
import fr.zom.csmm4.items.ItemCSMM;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, modid = ModCSMM.MODID)
public class ModItems
{

	public static final ModItems INSTANCE = new ModItems();

	public static List<Item> items;

	public static Item diamond_stick;

	private static void init()
	{
		items = Lists.newArrayList();

		diamond_stick = new ItemCSMM("diamond_stick");

	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> e)
	{
		init();

		for ( Item item : items )
		{
			e.getRegistry().register(item);
		}
	}

}
